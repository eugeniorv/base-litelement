import { LitElement, html } from "lit-element";

class PersonaDm extends LitElement{

  static get properties(){
    return {
      people: {type: Array},
    };
  }

  constructor(){
    super();

    this.people = [
      {
        name: "Nombre 1",
        yearsInCompany: 10,
        photo: {
          src: "./img/persona.jpeg",
          alt: "Nombre 1"
        },
        profile: "Lorem ipsum dolor sit amet 1."
      },{
        name: "Nombre 2",
        yearsInCompany: 2,
        photo: {
          src: "./img/persona.jpeg",
          alt: "Nombre 2"
        },
        profile: "Lorem ipsum dolor sit amet 2."
      },{
        name: "Nombre 3",
        yearsInCompany: 5,
        photo: {
          src: "./img/persona.jpeg",
          alt: "Nombre 3"
        },
        profile: "Lorem ipsum dolor sit amet 3."
      },{
        name: "Nombre 4",
        yearsInCompany: 15,
        photo: {
          src: "./img/persona.jpeg",
          alt: "Nombre 3"
        },
        profile: "Lorem ipsum dolor sit amet 4."
      },{
          name: "Nombre 5",
          yearsInCompany: 1,
          photo: {
            src: "./img/persona.jpeg",
            alt: "Nombre 3"
          },
          profile: "Lorem ipsum dolor sit amet 5."
        }
    ]
  }

  updated(changedProperties){
    console.log("updated");

    if (changedProperties.has("people")){
      console.log("ha cambiado valor de people");
      this.dispatchEvent(
        new CustomEvent(
          "people-data-updated",
          {
            detail :{
              people : this.people
            }
          }
        )
      );
    }

  }

}

customElements.define('persona-dm', PersonaDm);
