import { LitElement, html } from "lit-element";
import '../persona-ficha-listado/persona-ficha-listado.js';
import '../persona-form/persona-form.js';
import '../persona-dm/persona-dm.js'

class PersonaMain extends LitElement{

  /*
  static get styles(){
    return css`
      :host {
        all: initial
      }
    `;
  }
  */
  static get properties(){
    return {
      people: {type: Array},
      showPersonForm: {type: Boolean}
    };
  }

  constructor(){
    super();

    this.people = [];

    this.showPersonForm = false;
  }

  render(){
    return html`
      <persona-dm
        @people-data-updated="${this.peopleDataUpdated}"
      ></persona-dm>

      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
      <h2 class="text-center">Persona Main</h2>
        <div class="row" id="peopleList">
          <div class="row row-cols-1 row-cols-sm-4">
            ${this.people.map(
              person => html`<persona-ficha-listado
                fname="${person.name}"
                yearsinCompany="${person.yearsInCompany}"
                .photo="${person.photo}"
                profile="${person.profile}"
                @delete-person="${this.deletePerson}"
                @info-person="${this.infoPerson}"
              ></persona-ficha-listado>`
            )}
          </div>
        </div>

        <div class="row">
          <persona-form
            @person-form-close="${this.personFormClose}"
            @person-form-store="${this.personFormStore}"
            class="d-none border rounded border-primary"
            id="personForm">
          </persona-form>
        </div>
    `;
  }

  updated(changedProperties){
    console.log("updated");

    if (changedProperties.has("showPersonForm")){
      console.log("Ha cambiado el valor de la propiedad showPersonForm en persona-main");

      if (this.showPersonForm === true){
        this.showPersonFormData();
      }else{
        this.showPersonList();
      }
    }

    if (changedProperties.has("people")){
      console.log("Ha cambiado el valor de la propiedad people en persona-main");

      this.dispatchEvent(
        new CustomEvent(
          "updated-people",
          {
            detail :{
              people : this.people
            }
          }
        )
      )
    }
  }

  peopleDataUpdated(e){
    console.log("peopleDataUpdated en persona-main");
    this.people = e.detail.people;
  }

  infoPerson(e){
    console.log("infoPerson en persona-main");
    console.log("Se ha pedido más información de la persona " + e.detail.name);

    let chosenPerson = this.people.filter(
      person => person.name === e.detail.name
    );

    let person = {};
    person.name = chosenPerson[0].name;
    person.profile = chosenPerson[0].profile;
    person.yearsInCompany = chosenPerson[0].yearsInCompany;

    this.shadowRoot.getElementById("personForm").person = person;
    this.shadowRoot.getElementById("personForm").editingPerson = true;
    this.showPersonForm = true;

  }

  personFormClose(){
    console.log("personFormClose");
    console.log("Se ha cerrado el formulario de la persona");

    this.showPersonForm = false;
  }

  personFormStore(e){
    console.log("personFormStore");
    console.log("Se va a almacenar una persona");
    console.log(e.detail.person);

    if (e.detail.editingPerson === true){
      console.log("Se va a actualizar la persona de nombre " +
        e.detail.editingPerson);

        this.people = this.people.map(
          person => person.name === e.detail.person.name
            ? person = e.detail.person : person
        );

      /* Esta forma no lanza el updated con people
      let indexOfPerson = this.people.findIndex(
        person => person.name === e.detail.person.name
      );

      if (indexOfPerson >= 0){
        console.log ("Persona encontrada");
        this.people[indexOfPerson] = e.detail.person;
      }
*/
    }else{
      console.log("Se va a alamacenar una persona nueva");

      this.people = [...this.people, e.detail.person];

      /* Esta forma no lanza el updated con people
      this.people.push(e.detail.person);
      */
    }

    console.log("Persona almacenada");

    this.showPersonForm = false;
  }

  showPersonFormData(){
    console.log("showPersonFormData");
    console.log("Mostrando formulario de persona");
    this.shadowRoot.getElementById("personForm").classList.remove("d-none");
    this.shadowRoot.getElementById("peopleList").classList.add("d-none");
  }

  showPersonList(){
    console.log("showPersonList");
    console.log("Mostrando listado de personas");
    this.shadowRoot.getElementById("personForm").classList.add("d-none");
    this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
  }

  deletePerson(e){
    console.log("deletePerson en persona-main");
    console.log("Se va a borrar la persona " + e.detail.name);

    this.people = this.people.filter(
      person => person.name != e.detail.name
    );
  }

}

customElements.define('persona-main', PersonaMain);
